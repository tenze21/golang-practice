package findmax

func FindMax(slice []int) int {
	var maxValue = slice[0]
	for i := 0; i < len(slice)-1; i++ {
		if maxValue < slice[i+1] {
			maxValue = slice[i+1]
		}
	}
	return maxValue
}
