package reverseslice

func ReverseSlice(slice []int) []int {
	reversedSlice := []int{}
	for i := len(slice); i > 0; i-- {
		reversedSlice = append(reversedSlice, slice[i-1])
	}
	return reversedSlice
}
