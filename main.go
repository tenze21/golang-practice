package main

func main() {
	// myArr := [...]string{"g", "o", "l", "a", "n", "g"}
	// mySlice := myArr[2:4]
	// fmt.Println(myArr)
	// fmt.Println(mySlice)
	// fmt.Println(len(mySlice))
	// fmt.Printf("%T\n", myArr)
	// fmt.Printf("%T\n", mySlice)

	// var s []int
	// fmt.Printf("%T\n", s)
	// s = make([]int, 0)
	// s = append(s, 1)
	// s = append(s, 2)
	// fmt.Println(s)

	// cards := []string{"Ace of spades", newCard()}
	// cards = append(cards, "Six of spades")
	// fmt.Println(cards)
	// fmt.Println(cap(cards))

	//normal for loop
	// for i := 0; i <= len(cards); i++ {
	// 	fmt.Println(cards[i])
	// }

	//usign range keyword
	// for i, card := range cards {
	// 	fmt.Println(i, card)
	// }

	//Create a function that takes a slice of integers and reverses it
	// mySlice := []int{1, 2, 3, 4, 5}
	// reversed := reverseslice.ReverseSlice(mySlice)
	// fmt.Println(reversed)

	//write a go program that takes a slice containing the names of fruits and prints the names of fruits with their index.
	// fruits := []string{"apple", "mango", "banana"}
	// for i, fruit := range fruits {
	// 	fmt.Println("index: ", i, "fruit: ", fruit)
	// }

	//Write a go program to get the maximum value in the slice of intergers
	// mySlice := []int{10, 40, 20, 50, 25, 70}
	// fmt.Println(findmax.FindMax(mySlice))

}

//	func newCard() string {
//		return "Five of Diamonds"
//	}
